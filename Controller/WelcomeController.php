<?php

namespace Controller;

class WelcomeController
{
    public function __construct()
    {
		
    }

	/**
    * GET welcome
    */
    public function index()
    {
        $id = $_GET['id'];
        $name = $_GET['name'];

        echo 'Hello World!' . $id . $name;
    }

	/**
    * GET welcome/{id:[0-9]+}
    */
    public function getId($id)
    {
        echo $id;
	}
	
	/**
    * POST welcome/{id:[0-9]+}
    */
    public function postId($id)
    {
        echo $id;
	}

	/**
	 * 	POST welcome/json
	 */
	public function postJson()
    {
		$content = file_get_contents('php://input');
		echo $content;
	}

	/**
	 * GET /
	 */
	public function rootTest()
	{ 	
		echo 'this is root';
	}
}
