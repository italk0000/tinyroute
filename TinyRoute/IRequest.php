<?php

namespace TinyRoute;

interface IRequest
{
    public function getBody();
}