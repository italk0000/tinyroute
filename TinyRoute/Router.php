<?php

namespace TinyRoute;

use Closure;
use ArrayAccess;
use ReflectionClass;
use ReflectionMethod;
use ReflectionFunction;
use ReflectionParameter;

use TinyRoute\Helper\RouterHelper;

class Router
{
    private $request;
    private $nameSpace;
    private $regexBasedAbstract;
    private $methodToRegexToRoutesMap = [];

    public function __construct(IRequest $request)
    {
        $this->request = $request;
        $this->regexBasedAbstract = new Core\RegexBasedAbstract();
    }

    public function group($nameSpace, $callback)
    {
        $this->nameSpace = $nameSpace;
        call_user_func($callback, $this);
    }

    public function __call($httpMethod, $args)
    {
        if (!isset($this->nameSpace)) {
            die();
        }

        list($route, $action) = $args;

        $routeDatas = RouterHelper::parseRoute($route);
        foreach ($routeDatas as $routeData) {
            list($regex, $variables) = RouterHelper::buildRegexForRoute($routeData);
            $handler = $this->nameSpace . '\\' . $action;
            $this->methodToRegexToRoutesMap[$httpMethod][$regex] = new Model\RouteReg(
                $httpMethod, $handler, $regex, $variables
            );
        }
    }

    /**
     * Resolves a route
     */
    public function resolve()
    {
        $pathInfo = parse_url($this->request->requestUri);
        $uriPath = $pathInfo['path'];

        $root = $_SERVER['DOCUMENT_ROOT'];
        $appRoot = dirname(dirname(__FILE__));

        $appPath = str_replace($root, '', $appRoot);
        $appPathArr = explode(DIRECTORY_SEPARATOR, $appPath);
        for ($i = 0; $i < count($appPathArr); $i++) {
            $p = $appPathArr[$i];
            if ($p) {
                $uriPath = preg_replace('/^\/' . $p . '\//', '/', $uriPath, 1);
            }
        }

        // remove first slash
        $uriPath = preg_replace('/^\//', '', $uriPath, 1);

        $result = $this->searchRouteData($uriPath);
        if ($result[0]) {
            list($controller, $method) = explode('@', $result[1]);

            // $isClosure = $controller instanceof Closure;
            // $reflector = new ReflectionClass($controller);
            // $instance = $reflector->newInstanceArgs([$this->request]);

            call_user_func_array([new $controller, $method], $result[2]);
        } else {
            header("{$this->request->serverProtocol} 404 Not Found");
            die();
        }
    }

    private function searchRouteData($uriPath)
    {
        $routeDatas = $this->regexBasedAbstract->generateVariableRouteData($this->methodToRegexToRoutesMap);
        $routeData = $routeDatas[strtolower($this->request->requestMethod)];
        foreach ($routeData as $data) {
            if (!preg_match($data['regex'], $uriPath, $matches)) {
                continue;
            }

            list($handler, $varNames) = $data['routeMap'][count($matches)];

            $vars = [];
            $i = 0;
            foreach ($varNames as $varName) {
                $vars[$varName] = $matches[++$i];
            }
            return [true, $handler, $vars];
        }
        return [false];
    }
}
