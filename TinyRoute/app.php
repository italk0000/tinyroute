<?php

require_once __DIR__ . '/Core/AutoLoad.php';

$router = new TinyRoute\Router(new TinyRoute\Request);

return $router;