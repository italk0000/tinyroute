<?php

function autoload($className)
{
    $className = ltrim($className, '/');
    $fullPath = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
    if (is_readable($fullPath)) {
        require_once $fullPath;
    }
}

spl_autoload_register('autoload', true, true);
