<?php

$router = require_once __DIR__ . '/TinyRoute/app.php';

/**
 * Example:
 * 
 * $router->group({ControllerPath}, function($router) {
 *      $router->get({Uri}, {ControllerName}@{FunctionName});
 * }
 * 
 */

$router->group('Controller', function($router) {
    $router->get('welcome', 'WelcomeController@index');
    $router->get('welcome/{id:[0-9]+}', 'WelcomeController@getId');
    $router->post('welcome/{id:[0-9]+}', 'WelcomeController@postId');
    $router->post('welcome/json', 'WelcomeController@postJson');
    $router->get('', 'WelcomeController@rootTest');
});

// Parser Uri
$router->resolve();